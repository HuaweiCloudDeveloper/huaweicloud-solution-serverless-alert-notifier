[TOC]

**解决方案介绍**
===============
该方案基于无服务器架构，快速构建一个一键自动部署的无服务器告警推送解决方案，实现将华为云的资源告警信息推送到客户指定的通知平台（如企业微信）。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/serverless-alert-notifier.html

**架构图**
---------------
![方案架构](./document/serverless-alert-notifier.png)

**架构描述**
---------------
该解决方案会部署如下资源：
1. 云监控服务CES，为Region内所有运行中的ECS的CPU、内存、磁盘使用情况生成对应的告警规则，当ECS的状态变化触发告警规则设置的阈值时，您将收到告警通知。
2. 消息通知服务SMN，用于接受来自CES的告警数据，并触发函数工作流FunctionGraph进行告警推送。
3. 函数工作流 FunctionGraph，该方案利用FunctionGraph调用微信接口推送告警信息，以SMN主题作为触发器。
4. 数据加密服务DEW，该方案利用DEW存储企业微信的凭证数据，使得FunctionGraph可以安全地访问这些数据，轻松实现对敏感凭据的全生命周期统一管理。
5. 此外，您也可以自己创建其他告警规则并添加到该方案自动生成的消息通知服务SMN中，实现自定义告警推送。

**组织结构**
---------------
``` lua
huaweicloud-solution-serverless-alert-notifier
├── serverless-alert-notifier.tf.json -- 资源编排模板
```
**开始使用**
---------------

部署完成后，您可以通过从SMN发送测试消息，来测试微信是否可以成功接收到告警信息。

1、进入消息通知服务SMN控制台，进入到“主题管理”下的“主题”页面。找到对应的主题名称，单击操作列的发布消息。

图1 主题管理

![主题管理](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/serverless-alert-notifier/readme/readme-image-001.jpg)

2、填写主题和消息内容，单击下方的“确认”，就可以将消息发布到SMN主题，自动触发FunctionGraph将消息发送给微信平台。如果之前获取的凭证无误，此时您的微信会提醒您收到一条新消息。

图2 发布消息

![发布消息](https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/serverless-alert-notifier/readme/readme-image-002.jpg)




